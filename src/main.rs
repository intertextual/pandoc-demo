use pandoc_types::definition::{Block::{self,*}, Inline::{self, *}, Pandoc, QuoteType, Target};

fn main() -> std::io::Result<()> {
    let ast: Pandoc = serde_json::from_reader(std::io::stdin()).expect("failed to parse pandoc AST");

    write_pandoc(&mut std::io::stdout(), ast)
}

fn write_pandoc(output: &mut dyn std::io::Write, ast: Pandoc) -> std::io::Result<()> {
    match ast {
        Pandoc(_, blocks) => {
            let mut notes = Vec::<Vec<u8>>::new();
            write_blocks(output, &mut notes, blocks, None)?;
            if !notes.is_empty() {
                writeln!(output, "<hr />\n<ol>")?;
                for (i, note) in notes.into_iter().enumerate() {
                    let num = i+1;
                    writeln!(output, "<li id=\"fn{}\">", num)?;
                    output.write_all(&note)?;
                    writeln!(output, " <a href=\"#fnref{}\" class=\"footnote-back\">↩</a>\n</li>", num)?;
                }
                writeln!(output, "</ol>")?;
            }
        }
    }

    Ok(())
}

fn write_blocks(output: &mut dyn std::io::Write, notes: &mut Vec<Vec<u8>>, blocks: Vec<Block>, tag: Option<&str>) -> std::io::Result<()> {
    if let Some(s) = tag {
        writeln!(output, "<{}>", &s)?;
    }

    for block in blocks.into_iter() {
        match block {
            Plain(inlines) => {
                write_inlines(output, notes, inlines, None)?;
                writeln!(output)?;
            }
            Para(inlines) => {
                write_inlines(output, notes, inlines, Some("p"))?;
                writeln!(output)?;
            }
            LineBlock(inlineses) => {
                writeln!(output, "<p>")?;
                for (i, inlines) in inlineses.into_iter().enumerate() {
                    if i != 0 {
                        write!(output, "<br />")?;
                    }
                    write_inlines(output, notes, inlines, None)?;
                }
                writeln!(output, "</p>")?;
            }
            CodeBlock(_, s) => {
                writeln!(output, "<pre>")?;
                write_escaped_cdata(output, s)?;
                writeln!(output, "</pre>")?;
            }
            BlockQuote(blocks) => {
                write_blocks(output, notes, blocks, Some("blockquote"))?;
            }
            OrderedList(_, blockses) => {
                writeln!(output, "<ol>")?;
                for blocks in blockses.into_iter() {
                    write_blocks(output, notes, blocks, Some("li"))?;
                }
                writeln!(output, "</ol>")?;
            }
            BulletList(blockses) => {
                writeln!(output, "<ul>")?;
                for blocks in blockses.into_iter() {
                    write_blocks(output, notes, blocks, Some("li"))?;
                }
                writeln!(output, "</ul>")?;
            }
            DefinitionList(entries) => {
                writeln!(output, "<dl>")?;
                for (inlines, blockses) in entries.into_iter() {
                    write_inlines(output, notes, inlines, Some("dt"))?;
                    for blocks in blockses.into_iter() {
                        write_blocks(output, notes, blocks, Some("dd"))?;
                    }
                }
                writeln!(output, "</dl>")?;
            }
            Header(level, _, inlines) => {
                write_inlines(output, notes, inlines, Some(&format!("h{}", level)))?;
                writeln!(output)?;
            }
            HorizontalRule => {
                writeln!(output, "<hr />")?;
            }
            Div(_, blocks) => {
                write_blocks(output, notes, blocks, Some("div"))?;
            }
            Null => (),

            // Unsupported
            RawBlock(_, _) => (),
            Table(_, _, _, _, _) => (),
        }
    }

    if let Some(s) = tag {
        writeln!(output, "</{}>", &s)?;
    }

    Ok(())
}

fn write_inlines(output: &mut dyn std::io::Write, notes: &mut Vec<Vec<u8>>, inlines: Vec<Inline>, tag: Option<&str>) -> std::io::Result<()> {
    if let Some(s) = tag {
        write!(output, "<{}>", &s)?;
    }

    for inline in inlines.into_iter() {
        match inline {
            Str(s) => {
                write_escaped_cdata(output, s)?;
            }
            Emph(inlines) => {
                write_inlines(output, notes, inlines, Some("em"))?;
            }
            Strong(inlines) => {
                write_inlines(output, notes, inlines, Some("strong"))?;
            }
            Strikeout(inlines) => {
                write_inlines(output, notes, inlines, Some("s"))?;
            }
            Superscript(inlines) => {
                write_inlines(output, notes, inlines, Some("sup"))?;
            }
            Subscript(inlines) => {
                write_inlines(output, notes, inlines, Some("sub"))?;
            }
            SmallCaps(inlines) => {
                write!(output, "<span style=\"font-variant: small-caps;\">")?;
                write_inlines(output, notes, inlines, None)?;
                write!(output, "</span>")?;
            }
            Quoted(qt, inlines) => {
                let (lq, rq) = match qt {
                    QuoteType::SingleQuote => ("&lsquo;", "&rsquo;"),
                    QuoteType::DoubleQuote => ("&ldquo;", "&rdquo;"),
                };
                write!(output, "{}", lq)?;
                write_inlines(output, notes, inlines, None)?;
                write!(output, "{}", rq)?;
            }
            Cite(_, inlines) => {
                write_inlines(output, notes, inlines, Some("cite"))?;
            }
            Code(_, s) => {
                write!(output, "<code>")?;
                write_escaped_cdata(output, s)?;
                write!(output, "</code>")?;
            }
            Space => {
                write!(output, " ")?;
            }
            SoftBreak => {
                writeln!(output, "<br />")?;
            }
            LineBreak => {
                writeln!(output, "<br />")?;
            }
            Link(_, inlines, Target(url, title)) => {
                write!(output, "<a href=\"")?;
                write_safe_href(output, url)?;
                write!(output, "\" title=\"")?;
                write_escaped_attr(output, title)?;
                write!(output, "\">")?;
                write_inlines(output, notes, inlines, None)?;
                write!(output, "</a>")?;
            }
            Note(blocks) => {
                write!(output, "<a href=\"#fn{0}\" class=\"footnote-ref\" id=\"fnref{0}\"><sup>[{0}]</sup></a>", notes.len() + 1)?;
                notes.push(Vec::<u8>::new());
                let mut notebuf = Vec::<u8>::new();
                write_blocks(&mut notebuf, notes, blocks, None)?;
                let idx = notes.len() - 1;
                notes[idx].append(&mut notebuf);
            }
            Span(_, inlines) => {
                write_inlines(output, notes, inlines, Some("span"))?;
            }

            // Unsupported
            Math(_, _) => (),
            RawInline(_, _) => (),
            Image(_, _, _) => (),
        }
    }

    if let Some(s) = tag {
        write!(output, "</{}>", &s)?;
    }

    Ok(())
}

fn write_escaped_cdata(output: &mut dyn std::io::Write, s: String) -> std::io::Result<()> {
    write!(output, "{}", s.replace("&", "&amp;").replace("\"", "&quot;").replace("<", "&lt;").replace(">", "&gt;"))?;

    Ok(())
}

fn write_escaped_attr(output: &mut dyn std::io::Write, s: String) -> std::io::Result<()> {
    write!(output, "{}", s.replace("&", "&amp;").replace("\"", "&quot;"))?;

    Ok(())
}

fn write_safe_href(output: &mut dyn std::io::Write, s: String) -> std::io::Result<()> {
    if !(s.starts_with("http://") || s.starts_with("https://") || s.starts_with("mailto:")) {
        if !s.contains(":") {
            // someone forgot their protocol string, assume it's https and add it for them
            write!(output, "https://")?;
        } else {
            return Ok(());
        }
    }

    write!(output, "{}", s.replace("\"", "%22"))?;

    Ok(())
}
